module TestHelpers

open System.Text.RegularExpressions
open System

let compareStringsNoWhitespace stringOne stringTwo =
    String.Equals(Regex.Replace(stringOne, @"\s+", String.Empty),
     Regex.Replace(stringTwo, @"\s+", String.Empty))